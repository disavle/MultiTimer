# MultiTimer
## Testing task for SFERA

<img src="https://raw.githubusercontent.com/disavle/MultiTimer/master/ForDescription/ScreenShot.png" alt="drawing" width="200"/>

### UI
- Title
- Two customs view of the separators, including title
- Two textfields: 
  -  Name of the timer
  - Duration of seconds 
- The button
- Custom TableView

### Logic
* Touching the button, there adds cell in the TableView and sets the timer.
* Adding a new timer, there sorts in descending order.
* Timer completing removes cell with itself.
* Touching the cell pause/play the timer.


