//
//  Colors.swift
//  SFERA_Timer
//
//  Created by Dima Savelyev on 02.07.2021.
//

import UIKit

extension UIColor{
    static var grey1: UIColor{
        return UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1)
    }
    static var grey2: UIColor{
        return UIColor(red: 163/255, green: 163/255, blue: 167/255, alpha: 1)
    }
}
