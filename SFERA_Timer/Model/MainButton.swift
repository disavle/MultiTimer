//
//  MainButton.swift
//  SFERA_Timer
//
//  Created by Dima Savelyev on 02.07.2021.
//

import UIKit

class MainButton: UIButton {
    
    var button: UIButton!

    override init(frame: CGRect) {
        super.init(frame: frame)
        button = {
            let button = UIButton()
            button.layer.cornerRadius = 10
            button.setTitleColor(.systemBlue, for: .normal)
            button.backgroundColor = .systemGray6
            button.setTitle("Добавить", for: .normal)
            return button
        }()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
