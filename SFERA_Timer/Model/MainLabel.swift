//
//  MainLabel.swift
//  SFERA_Timer
//
//  Created by Dima Savelyev on 02.07.2021.
//

import UIKit
import SnapKit

class MainLabel{
    var label: UILabel!
    var text: String!
    var view: UIView!
    
    init(_ text: String, _ view: UIView) {
        self.text = text
        self.view = view
        label = {
            let label = UILabel()
            label.text = "      \(text)"
            label.backgroundColor = .grey1
            label.textColor = .grey2
            view.addSubview(label)
            
            let bottomBorder = UIView() // Bottom Border
            bottomBorder.backgroundColor = .grey2
            label.addSubview(bottomBorder)
            bottomBorder.snp.makeConstraints { maker in
                maker.centerX.equalToSuperview()
                maker.width.equalToSuperview()
                maker.height.equalTo(1)
                maker.bottom.equalTo(label)
            }
            return label
        }()
    }
}
