//
//  MainTextField.swift
//  SFERA_Timer
//
//  Created by Dima Savelyev on 02.07.2021.
//

import UIKit
import SnapKit

class MainTextField: UITextField{
    var field: UITextField!
    var holder: String!
    var view: UIView!
    var height: Double!
    
    init(_ frame: CGRect, _ text: String, height: Double, _ view: UIView) {
        super.init(frame: frame)
        self.text = holder
        self.height = height
        self.view = view
        field = {
            let field = UITextField()
            field.backgroundColor = .grey1
            field.placeholder = text
            field.layer.borderWidth = 1
            field.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
            field.returnKeyType = .done
            field.autocapitalizationType = .words
            field.autocorrectionType = .no
            field.layer.borderColor = CGColor(red: 163/255, green: 163/255, blue: 167/255, alpha: 1)
            field.layer.cornerRadius = 5
            view.addSubview(field)
            field.snp.makeConstraints { maker in
                maker.top.equalTo(view.snp_topMargin).inset(height)
                maker.left.equalTo(view.snp_leftMargin).offset(20)
                maker.height.equalTo(30)
                maker.width.equalTo(view).inset(85)
            }
            return field
        }()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
