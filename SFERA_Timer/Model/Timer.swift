//
//  Timer.swift
//  SFERA_Timer
//
//  Created by Dima Savelyev on 03.07.2021.
//

import UIKit

class TimerInit{
    
    var seconds: Int
    var timer: Timer!
    var min: Int!
    var sec: Int!
    
    init(seconds: Int) {
        self.seconds = seconds
        min = (seconds % 3600) / 60
        sec = (seconds % 3600) % 60
    }
    
    func setTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTImer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTImer(){
        seconds -= 1
        sec -= 1
        if (seconds == 0){
            timer.invalidate()
            MainViewController.shared.table.deleteCell()
        }
        if (sec < 0){
            sec = 59
            if (min != 0){
                min -= 1
            }
        }
        MainViewController.shared.table.update()
    }
}


