//
//  AddTimerView.swift
//  SFERA_Timer
//
//  Created by Dima Savelyev on 02.07.2021.
//

import UIKit

class AddTimerView: UIView, UITextFieldDelegate {
    
    var name: MainTextField!
    var time: MainTextField!
    var button: MainButton!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        name = MainTextField(self.frame, "Название таймера", height: 0, self)
        name.field.delegate = self
        
        time = MainTextField(self.frame, "Время в секундах", height: name.height+50, self)
        time.field.keyboardType = .decimalPad
        time.field.delegate = self
        
        button = MainButton(frame: self.frame)
        addSubview(button.button)
        button.button.snp.makeConstraints { maker in
            maker.top.equalTo(time.field.snp.bottom).offset(30)
            maker.centerX.equalToSuperview()
            maker.height.equalTo(50)
            maker.width.equalToSuperview().inset(20)
        }
        button.button.addTarget(self, action: #selector(updateField), for: .touchUpInside)
    }
    
    @objc func updateField(){
        MainViewController.shared.updateField()
        MainViewController.shared.add.name.field.text = ""
        MainViewController.shared.add.time.field.text = ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case name.field!:
            time.field!.becomeFirstResponder()
            default:
                time.field!.resignFirstResponder()
            }
        return true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
