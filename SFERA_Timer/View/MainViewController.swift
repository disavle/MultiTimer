//
//  MainViewController.swift
//  SFERA_Timer
//
//  Created by Dima Savelyev on 02.07.2021.
//

import UIKit
import SnapKit

class MainViewController: UIViewController {
    
    static let shared = MainViewController()

    var delegate: FieldDelegate?
    var add: AddTimerView!
    var table: TimersTableView!
    var addTimerLabel: MainLabel!
    var timersLabel: MainLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Мульти таймер"
        view.backgroundColor = .systemBackground
        
        addTimerLabel = MainLabel("Добавление таймеров", view)
        addTimerLabel.label.snp.makeConstraints { maker in
            maker.centerX.equalToSuperview()
            maker.top.equalTo(view.snp_topMargin).offset(20)
            maker.width.equalToSuperview()
            maker.height.equalTo(50)
        }
        
        add = {
            let add = AddTimerView()
        view.addSubview(add)
        add.snp.makeConstraints { maker in
            maker.top.equalTo(addTimerLabel.label).inset(70)
            maker.left.equalToSuperview()
            maker.height.equalTo(200) 
            maker.width.equalToSuperview()
        }
            return add
        }()
        
        MainViewController.shared.add = add
        
        timersLabel = MainLabel("Таймеры", view)
        timersLabel.label.snp.makeConstraints { maker in
            maker.centerX.equalToSuperview()
            maker.top.equalTo(add.snp.bottom)
            maker.width.equalToSuperview()
            maker.height.equalTo(addTimerLabel.label)
        }
        
        table = {
        let table = TimersTableView()
        view.addSubview(table)
        table.snp.makeConstraints { maker in
            maker.top.equalTo(timersLabel.label.snp.bottom)
            maker.left.equalToSuperview()
            maker.height.equalToSuperview()
            maker.width.equalToSuperview()
        }
            return table
        }()
        
        delegate = table
        MainViewController.shared.delegate = delegate
        MainViewController.shared.table = table
        
    }
    
    func updateField(){
        delegate?.addTimer(name: add.name.field.text!, time: Int(add.time.field.text!) ?? 0)
        add.time.field.resignFirstResponder()
    }
}

protocol  FieldDelegate{
    var timers: [String:Int] {get set}
    
    func addTimer(name: String, time: Int)
}


