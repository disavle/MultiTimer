//
//  TimersTableView.swift
//  SFERA_Timer
//
//  Created by Dima Savelyev on 02.07.2021.
//

import UIKit

class TimersTableView: UITableView, UITableViewDelegate, UITableViewDataSource, FieldDelegate {
    
    static let shared = TimersTableView()

    var names = [String]()
    var timers = [String:Int]()
    var cell: TimersTableViewCell!
    var timersTime = [TimerInit]()
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        self.delegate = self
        self.dataSource = self
        register(TimersTableViewCell.self, forCellReuseIdentifier: TimersTableViewCell.id)
        self.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        cell = self.dequeueReusableCell(withIdentifier: TimersTableViewCell.id, for: indexPath) as? TimersTableViewCell
        cell.labelName.text = names[indexPath.row]
        cell.labelTime.text = "\(timersTime[indexPath.row].min!):\(timersTime[indexPath.row].sec!)"
        return cell
    }
    
    func addTimer(name: String, time: Int) {
        timers.updateValue(time, forKey: name)
        names.append(name)
        timersTime.append(TimerInit(seconds: time))
        timersTime.last?.setTimer()
        sort()
        reloadData()
    }
    
    func deleteCell(){
        names.removeLast()
        deleteRows(at: [IndexPath(row: names.count, section: 0)], with: .fade)
        update()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (timersTime[indexPath.row].timer.isValid){
            timersTime[indexPath.row].timer.invalidate()
        } else {
            timersTime[indexPath.row].setTimer()
        }
    }
    
    func sort(){
        guard names.count>1 else {
            return
        }
        for i in 0..<names.count-1{
            for j in 0..<names.count-i-1{
                if (timersTime[j].seconds < timersTime[j+1].seconds){
                    let temp = timersTime[j]
                    timersTime[j] =  timersTime[j+1]
                    timersTime[j+1] = temp
                    let temp2 = names[j]
                    names[j] = names[j+1]
                    names[j+1] = temp2
                }
            }
        }
    }
    
    func update(){
        sort()
        reloadData()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
