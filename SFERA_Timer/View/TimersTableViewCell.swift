//
//  TimersTableViewCell.swift
//  SFERA_Timer
//
//  Created by Dima Savelyev on 02.07.2021.
//

import UIKit

class TimersTableViewCell: UITableViewCell {
    
    static let id = "cell"
    var labelName: UILabel!
    var labelTime: UILabel!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        labelName = {
            let label = UILabel()
            addSubview(label)
            label.frame = bounds
            return label
        }()
        
        labelTime = {
            let label = UILabel()
            label.textColor = .grey2
            addSubview(label)
            label.frame = bounds
            return label
        }()
        
        let stackView = UIStackView(arrangedSubviews: [labelName, labelTime])
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        addSubview(stackView)
        stackView.snp.makeConstraints { maker in
            maker.centerX.equalToSuperview()
            maker.width.equalToSuperview().inset(20)
            maker.centerY.equalToSuperview()
            maker.height.equalTo(self.bounds.height)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
